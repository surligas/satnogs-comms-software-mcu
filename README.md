# SatNOGS COMMS Software MCU

An open source CubeSat format communication sub-system.

## Development Guide
SatNOGS COMMS Software MCU in independent of any kind of development tool. 
You can use the development environment of your choice. 

However we recommend the usage of the STM32CubeIDE for the
development, debugging and experimentation.
This IDE has been optimized for the STM32 MCUs and provide helpful 
graphical debugging tools.
More information regarding the import process of the project into this tool 
can be found at [Import into the STM32CubeIDE](#Import into STM32CubeIDE) section.

### Requirements
* GNU Make
* cross-arm-none-eabi-gcc (>= 5.0)
* STM32 CubeMX (for modifying peripherals and FreeRTOS parameters)

### Dependencies
The SatNOGS COMMS Software MCU codebase depends on the drivers:
* [AT86RF215 Driver](https://gitlab.com/librespacefoundation/at86rf215-driver)
* [RFFCx07x Driver ](https://gitlab.com/librespacefoundation/rffcx07x-driver) 
 which are shipped as git submodule within the project.

### Coding Style
For the C code, we use a slightly modified version of the 
**Linux Kernel** style. Use `astyle` and the options file `.astylerc` to
adapt to the styling.

At the root directory of the project there is the `astyle` options 
file `.astylerc` containing the proper configuration.
Developers can import this configuration to their favorite editor. 
In addition the `hooks/pre-commit` file contains a Git hook, 
that can be used to perform before every commit, code style formatting
with `astyle` and the `.astylerc` parameters.
To enable this hook developers should copy the hook at their `.git/hooks` 
directory. 

### Import into STM32CubeIDE
- Download STM32CubeIDE
- Open the `satnogs-comms-software-mcu` directory and delete any possible instances of project configuration files (e.g `.project` or `.cproject` and such)
- Launch STM32CubeIDE and go to File -> Import -> Projects from Folder or Archive and find the source directory of satnogs-comms-software-mcu
- Open the `satnogs-comms-software-mcu.ioc` file. This will launch the Device Configuration Tool perspective
- Go to Project -> Generate Code. This will generate the necessary configuration files

Now some final steps are needed:

- Go to Project Properties. Expand the `C/C++ General` Tab. Go to `Paths and Symbols` and add the following directories from workspace: `Drivers/rffcx07x-driver/include`, 
`Drivers/at86rf215-driver/include` and `.` 

## Website and Contact
For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).

## License
![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png) 
&copy; 2019-2020 [Libre Space Foundation](https://libre.space).
